import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server:{
    open:false,
    proxy:{
      '/img':{
        target:'https://image.baidu.com',
        changeOrigin:true,
        rewrite:(path)=>path.replace(/^\/img/,'')
      }
    }
  },
  build:{
    outDir:'public'
  }
})
