const { contextBridge,ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('preload', {
  node: () => process.versions.node,
  chrome: () => process.versions.chrome,
  electron: () => process.versions.electron,
  click:(val)=>ipcRenderer.send(val),
  open:(url)=>ipcRenderer.send('open',url),
})