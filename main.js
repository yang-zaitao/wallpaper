process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';
const {Menu,app,ipcMain,nativeImage,Tray} = require('electron');
const { Save,createWin } = require('./mainbin');

const createView = (ObjURL,parent)=>{
    const win = createWin();
    win.setParentWindow(parent);
    const temple = Menu.buildFromTemplate([{label:'保存',click:()=>Save(ObjURL,'jpg',parent)}]);
    win.setMenu(temple);
    win.loadURL(ObjURL);
}

const MainWindow = ()=>{
    const win = createWin('hidden');
    // win.webContents.openDevTools();
    win.loadURL('http://localhost:5173/');
    ipcMain.on('minimize',()=>win.minimize());
    ipcMain.on('fullScreen',()=>win.setFullScreen(!win.fullScreen));
    ipcMain.on('quit',()=>app.quit());
    ipcMain.on('open',(ent,url)=>createView(url,win));
}
//准备好后创建窗口
app.whenReady().then(()=>{
    const icon = nativeImage.createFromPath('./src/assets/icon.png')
    let tray = new Tray(icon)
    tray.setToolTip('This is my application.')
    tray.setContextMenu(Menu.buildFromTemplate([
        {label:'退出',click:()=>app.quit()}
    ]))
    MainWindow()
});
//所有窗口都关闭了事件
app.on('window-all-closed', () => {
    //监测不是mac就用quit()
    if (process.platform !== 'darwin') app.quit()
})