import { PictureOutlined, UserOutlined } from '@ant-design/icons';
import { MenuProps,Menu} from 'antd';
type MenuItem = Required<MenuProps>['items'][number];
function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}
import { ReactElement ,useState} from 'react';
import Home from './view/Home';
import About from './view/About';

export default function App(): ReactElement{
    const [Index,setIndex] = useState<number>(0);
    const items: MenuProps['items'] = [
      getItem('壁纸', '0',<PictureOutlined />),
      getItem('我的', '1',<UserOutlined />),
    ];    
    const onClick: MenuProps['onClick'] = (e) => {
      if(String(Index) !== e.key){
        setIndex(Number(e.key));
      }
    };
    return <div className='Main'>
      <Menu
          onClick={onClick}
          style={{ width: 150}}
          defaultSelectedKeys={[String(Index)]}
          mode="inline"
          items={items}
      />
      <div className='Content'>
        {Number(Index) === 0 ? <Home/>:<About/> }
      </div>
    </div>
}