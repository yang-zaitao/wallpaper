interface Image{
    width?:number | string 
    height?:number |string,
    src:string,
    ObjURL:string,
    fromPageTitle?:string
}
import { DownloadOutlined,HeartFilled, HeartOutlined } from '@ant-design/icons';
import { Button, Modal,notification } from 'antd';
import { useState,useEffect } from 'react';
export default function Image({width,height,src,ObjURL,fromPageTitle}:Image){
    const [ModalShow,setModalShow] = useState<boolean>(false);
    const [love,setLove] = useState<boolean>(false);
    const openMessage = function(msg:string = 'helloworld'){
        notification.success({
            message: msg
        })
    }
    return <>
        <div className="image"
            onClick={()=>setModalShow(true)}>
            <div
                style={{
                width:'100%',
                height:'100%',
                backgroundImage:`url(${src})`,
                backgroundSize:'cover'
                }}>
            </div>
            <span>{fromPageTitle}</span>
        </div>
        <Modal
        title={fromPageTitle}
        style={{top:'5%'}}
        width={'60%'}
        mask={true}
        open={ModalShow} 
        onCancel={()=>setModalShow(false)}
        footer={[
            <Button 
            key={Math.random().toString(36)}
            icon={love ? 
            <HeartFilled style={{color:'red'}}  /> : <HeartOutlined />}
            onClick = {()=>null}
            >喜欢</Button>,
            <Button 
            key={Math.random().toString(36)}
            icon={<DownloadOutlined />} 
            onClick={()=>null}
            >下载</Button>,
            <Button 
            key={Math.random().toString(36)}
            icon={<DownloadOutlined />} 
            onClick={()=>{(window as any).preload.open(ObjURL)}}
            >预览原图</Button>,
        ]}
        >
        <img style={{width:'100%'}} srcSet={src} />    
        </Modal>
    </>
}