import type Imgs from '../type/Imgs';
import ajaxs from '../type/ajax.json';
import Image from '../components/Image';
import localStorage from '../type/localStorage';
import { useState,useEffect} from 'react';
import Search from 'antd/es/input/Search';
import {Switch } from 'antd';
//用来请求壁纸资源
export default function Home(){
    const [show,setShow] = useState<boolean>(false);
    const [imgs,setImgs] = useState<Imgs[]>([]);
    const getImgs = function(name:string = '壁纸') {
        let url = ajaxs[0].src;
        if((import.meta as any).env.PROD) url = ajaxs[0].url;
        fetch(`${url}/search/acjson?tn=resultjson_com&word=${name}`,{method:'GET'})
        .then(res=>res.json()
        .then(suc=>{
            if(suc.data){
                suc.data.pop();
                setImgs(suc.data);
                console.log();
                
            }
        }));
    }
    const onSearch = function(e:string){
        if(e.length>0){
            getImgs(e);
        }
    }
    useEffect(()=>{
        localStorage.init();
        getImgs();
    },[])
    return<div className='imgs'>
        <div className='sousuo'>
            <Switch onChange={e=>{setShow(e)}} size="small" />
            {show ? <Search
                className='Search'
                placeholder="查找你想要的壁纸"
                allowClear
                enterButton="搜索"
                size="large"
                onSearch={onSearch}
            />:null}
        </div>
        {imgs.map((item)=><Image 
        key={Math.random().toString(36)}
        fromPageTitle = {item.fromPageTitle}
        src={(item as any).thumbURL}
        ObjURL={(item as any).replaceUrl[0].ObjURL}
        width={item.width}
        height={item.height}
        />)}
    </div>
}