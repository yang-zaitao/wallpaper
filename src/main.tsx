import type Preload from "./type/preload";
import { BorderOutlined, CloseOutlined, MinusOutlined} from "@ant-design/icons";
import ReactDOM from "react-dom/client";
import React from "react";
import App from "./App";
import 'antd/dist/reset.css';
import './css/main.css';

const preload:Preload = (window as any).preload;

ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
.render(
    <React.StrictMode>
        <div className="Title">
            <img src={(import.meta as any).env.PROD ? "/public/assets/vite.svg":"/src/assets/vite.svg"} alt="" />
            <ul className="Btns">
                <button onClick={()=>preload.click('minimize')}><MinusOutlined /></button>
                <button onClick={()=>preload.click('fullScreen')}><BorderOutlined /></button>
                <button onClick={()=>preload.click('quit')}><CloseOutlined /></button>
            </ul>
        </div>
        <App/>
    </React.StrictMode>
);