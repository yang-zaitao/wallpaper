type fun = Function;
export default interface preload{
    node:fun,
    chrome:fun,
    electron:fun,
    click:fun,
    open:fun
}