export default class localStorage{
    static init(){
        const data = window.localStorage.getItem('data');
        if(data === null){
            window.localStorage.setItem('data','[]');
        }
        return JSON.parse((data as string));
    }
    static add(val:string){
        if(this.indexOf(val) !== true){
            const arrs:string[] = this.init();
            arrs.push(val);
            window.localStorage.setItem('data',JSON.stringify(arrs));
        }
        return this.init();
    }
    static del(val:string){
        if(this.indexOf(val)){
            const arrs:string[] = this.init();
            arrs.splice(arrs.indexOf(val),1);
            window.localStorage.setItem('data',JSON.stringify(arrs));
        }
        return this.init();
    }
    static indexOf(val:string){
        const arrs:string[] = this.init();
        if(arrs.indexOf(val) !== -1){
            return true;
        }else{
            return false;
        }
    }
}